import Post from "./pages/Post";
import Error from './pages/Error';
import Home from "./pages/Home";
import Limit from "./pages/Limit";

const rootRoutes = [
    {
        path: "/",
        component: Home,
        exact: true
    },
    {
        path: "/posts/:postId",
        exact: true,
        component: Post
    },
    {
        path: "/posts/limit/:count",
        exact: true,
        component: Limit
    },
    {
        component: Error
    }
];

export default rootRoutes;
