import React from "react";
import "./App.scss";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import {createBrowserHistory} from "history";
import Header from "./components/Header";
import rootRoutes from "./rootRoutes";

export const history = createBrowserHistory();
const supportsHistory = "pushState" in window.history;

function App() {
    return (
        <div className="App">
            <BrowserRouter basename="/" forceRefresh={!supportsHistory}>
                <Header/>
                <Switch>
                    {rootRoutes.map((route, key) => {
                        return <Route key={key} {...route} />;
                    })}
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
