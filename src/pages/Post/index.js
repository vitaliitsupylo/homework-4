import React, {Component} from "react";
import Loading from "./../../components/Loading";
import Button from "./../../components/Button";
import Comments from './../../components/Comments';

class PostPage extends Component {
    state = {
        post: null,
        comments: [],
        loading: false,
        loadingComments: false
    };

    componentDidMount() {
        const {match, history} = this.props;
        const {postId} = match.params;

        if (Number.isInteger(+postId)) {
            fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
                .then(res => res.json())
                .then(res => {
                    if (Object.keys(res).length > 0) {
                        this.setState({
                            post: res,
                            loading: true
                        });
                    } else {
                        history.push("/");
                    }
                });
        } else {
            history.push("/");
        }
    }

    loadComments = (postId) => () => {
        this.setState({
            loadingComments: true
        });
        fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    comments: res
                });
            });
    }

    render() {
        const {post, loading, comments, loadingComments} = this.state;
        const {loadComments} = this;
        return (
            <div className="page">
                <div className="page__wrap">
                    {!loading ?
                        <Loading/> :
                        <article className="post-full">
                            <h1 className="post-full__title">{post.title}</h1>
                            <p className="post-full__body">{post.body}</p>
                            {comments.length !== 0 || <Button handler={loadComments(post.id)}>View comments</Button>}
                            {!loadingComments || (comments.length) ? <Comments arrComments={comments}/> : <Loading/>}
                        </article>
                    }
                </div>
            </div>
        );
    }
}

export default PostPage;
