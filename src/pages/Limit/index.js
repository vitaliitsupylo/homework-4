import React, {Component} from "react";
import Post from "./../../components/Post";
import Loading from "./../../components/Loading";
import Button from "../../components/Button";

class Limit extends Component {
    state = {
        posts: [],
        limit: 0,
        step: 20,
        loading: false,
        more: false
    };

    componentDidMount() {
        const {match, history} = this.props;
        const {count} = match.params;

        if (Number.isInteger(+count)) {
            fetch(`https://jsonplaceholder.typicode.com/posts`)
                .then(res => res.json())
                .then(res => {
                    this.setState({
                        posts: res,
                        loading: true,
                        limit: +match.params.count,
                        more: res.length > this.state.limit
                    });
                });
        } else {
            history.push("/");
        }
    }

    loadMore = () => {
        const {posts, limit, step} = this.state;
        if (limit + step < posts.length) {
            this.setState({
                limit: limit + step
            })
        } else {
            this.setState({
                limit: posts.length,
                more: false
            })
        }
    };

    render() {
        const {posts, loading, limit, more} = this.state;
        const {loadMore} = this;
        const listPost = [];
        const count = posts.length && limit;

        for (let i = 0; i < count; i++) {
            listPost.push(
                <Post
                    title={posts[i].title}
                    body={posts[i].body}
                    postUrl={`/posts/${posts[i].id}`}
                    key={posts[i].id}
                    id={posts[i].id}
                />
            );
        }

        return (
            <div className="page">
                <div className="page__wrap">
                    <div className="page__list">
                        {!loading ? <Loading/> : listPost}
                    </div>
                    {more ? <Button handler={loadMore}>More</Button> : null}
                </div>
            </div>
        );
    }
}

export default Limit;
