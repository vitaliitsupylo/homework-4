import React from "react";
import "./style.scss";
import PropTypes from "prop-types";

const Button = ({type, children, handler}) => {
    return (
        <button type={type} className="button" onClick={handler}>
            <span>{children}</span>
        </button>
    );
};

export default Button;


Button.propTypes = {
    type: PropTypes.oneOf(['button', 'input', 'submit']).isRequired,
    children: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired
};


Button.defaultProps = {
    type: 'button',
    children: 'button',
    handler() {
        console.log('no handler')
    }
};