import React from "react";
import './style.scss';
import PropTypes from "prop-types";

const Comments = ({arrComments}) => {
    return (
        <ul className="comments">
            {arrComments.map((elem, item) => {
                return (<li key={item}>
                    <b>{elem.email}</b><br/>
                    {elem.body}
                </li>)
            })}
        </ul>
    );
};

export default Comments;

Comments.propTypes = {
    arrComments: PropTypes.array.isRequired
};


Comments.defaultProps = {
    arrComments: []
};