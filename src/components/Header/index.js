import React from "react";
import { NavLink, Link } from "react-router-dom";
import "./style.scss";

const Header = () => {
  return (
    <header className="header">
      <div className="header__wrap">
        <Link to="/" className="header__logo">
          Logo
        </Link>
        <nav className="header__nav">
          <NavLink exact className="header__nav-elem" to="/">
            Home
          </NavLink>
          <NavLink className="header__nav-elem" to="/posts/limit/15">
              Last 15 post
          </NavLink>
          <NavLink className="header__nav-elem" to="/posts/1">
            First post
          </NavLink>
        </nav>
      </div>
    </header>
  );
};

export default Header;
