import React from "react";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';
import "./style.scss";

const Post = ({title, body, id, postUrl}) => {
    return (
        <article className="post" data-id={id}>
            <h1 className="post__title">{title}</h1>
            <p className="post__body">{body}</p>
            {postUrl && (
                <Link to={`${postUrl}`} className="post__link">
                    <span>Details</span>
                </Link>
            )}
        </article>
    );
};

export default Post;


Post.propTypes = {
    title: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired
};


Post.defaultProps = {
    title: 'no title',
    body: 'no text',
    id: 1
};
